//
//  NSNumberExtensions.h
//  ListMonster
//
//  Created by Norm Barnard on 2/13/11.
//  Copyright 2011 clamdango.com. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSNumber(Extensions) 

- (id)initWithString:(NSString *)stringValue;
+ (NSNumber *)numberWithString:(NSString *)stringValue;

@end
